# -*- coding: utf-8 -*-
import urllib3
import json
import os
from bs4 import BeautifulSoup

def get_html(url):
    http = urllib3.PoolManager()
    ret = http.request("GET", url)
    return ret.data

def get_name_map_url(html):
    soup = BeautifulSoup(html, "lxml")
    ret = {}
    for line in soup.body.find_all("tr"):
        item = line.find_all("td")
        name = item[0].string.strip()
        url = item[1].string.strip()
        if "." not in url:
            continue
        print '{title:"%s"},' % (name)
        ret[name] = url
    return ret

def write_to_json(data, path, is_view=False):
    with open(path, "w") as wf:
        if is_view:
            wf.write(json.dumps(data, ensure_ascii=False, indent=4).encode("utf-8"))
        else:
            wf.write(json.dumps(data))

if __name__ == "__main__":
    url = "http://zuits.zju.edu.cn/domain/"
    path = os.path.join(os.getcwd(), "../data/name_map_url.json")
    html = get_html(url)
    name_map_url = get_name_map_url(html)
    write_to_json(name_map_url, path)
