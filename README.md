# 浙江大学网址导航搜索

## 安装
- 语言： python 3.5
- 用到的技术：
    - 爬虫：urllib, BeautifulSoup
    - 网页前端：CSS, HTML, JS, JQuery
    - 网页后端：Python, Flask, Levenshtein
- 安装依赖：
    - Flask: 轻量级的 web 服务器。`pip install Flask`
    - jieba: 分词工具。`pip install jieba`
    - BeautifulSoup: lxml 解析器。`pip install beautifulsoup4`
    - python-Levenshtein: 相似度计算包。[https://pypi.python.org/pypi/python-Levenshtein/0.12.0](https://pypi.python.org/pypi/python-Levenshtein/0.12.0) 下载安装包，然后解压进入目录`python3 setup.py install`

## 设计
- 爬虫
    - 使用 Python 模块 urllib3 抓取 [http://zuits.zju.edu.cn/domain/](http://zuits.zju.edu.cn/domain/) 网页的 HTML 源码
    - 使用 Beautiful 解析网页源码，抓取信息。
    - 对抓取的信息（网址名称，网址链接）用 json 保存。
- 网站
    - 总共两个页面：index.html, result.html
    - 对用户搜索的词，用 jieba 进行分词。
    - 对分好的词，查寻所有网址名称，看是否命中，如果命中，则保存到结果
    - 对所有结果与查询词进行相似度匹配，计算字符串的 ratio 距离，计算公式 $$r=\frac{(sum - ldist)}{sum}$$
    - 对结果按照相似度进行从大到小排序
    - 返回结果到客户端。
