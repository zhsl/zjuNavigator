# -*- coding: utf-8 -*-
import os
import json
import jieba
import Levenshtein
from flask import Flask, jsonify, render_template, request

def load_data(path):
    with open(path, "r") as rf:
        ret = json.loads(rf.read())
    return ret

name_map_url_path = os.path.join(os.getcwd(), "data/name_map_url.json")
name_map_url = load_data(name_map_url_path)
jieba.initialize()
app = Flask(__name__)
red_prefix = "<font color=\"#FF0000\">"
red_suffix = "</font>"

@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template("index.html")

@app.route('/result', methods=['GET', 'POST'])
def result():
    if request.method == 'GET':
        return render_template("index.html")
    else:
        search_content = request.form["search_content"]
        print (search_content)
        ret = get_search_result(search_content)
        wirte_html(ret, search_content)
        return render_template("result.html")

def wirte_html(ret, search_content):
    head_path = os.path.join(os.getcwd(), "templates/result_head.html")
    middle_path = os.path.join(os.getcwd(), "templates/result_middle.html")
    end_path = os.path.join(os.getcwd(), "templates/result_end.html")
    result_path = os.path.join(os.getcwd(), "templates/result.html")
    with open(head_path, "rb") as rf:
        head = rf.read().decode()
    with open(middle_path, "rb") as rf:
        middle = rf.read().decode()
    with open(end_path, "rb") as rf:
        end = rf.read().decode()
    with open(result_path, "w") as wf:
        wf.write(head + search_content + middle + ret + end)

def get_item_html(words, name, url):
    ret = "<li><a target=\"_blank\" href=http://%s>%s</a></li>\n"
    vis = [False] * len(name)
    for i in range(len(name)):
        for word in words:
            if name[i] in word:
                vis[i] = True
    new_name = ""
    i = 0
    while i < len(name):
        if vis[i]:
            new_name += red_prefix
            while i < len(name) and vis[i]:
                new_name += name[i]
                i += 1
            new_name += red_suffix
        else:
            new_name += name[i]
            i += 1
    return ret % (url, new_name)

def get_search_result(search_content):
    words = [ x for x in jieba.cut(search_content, cut_all=True)]
    print ("分词结果（全模式）：", ",".join(words))
    ret = []
    for name, url in name_map_url.items():
        flag = False
        for word in words:
            if word in name:
                flag = True
                break
        if flag:
            ret.append((Levenshtein.ratio(name, search_content), name, url))
    ret = sorted(ret, key=lambda x: x[0], reverse=True)
    ans = ""
    for x in ret:
        ans += get_item_html(words, x[1], x[2])
#        ans += "<li><a target=\"_blank\" href=http://%s>%s</a></li>\n" % (x[2], x[1])
    return ans

if __name__ == '__main__':
    app.debug=True
    app.run()
